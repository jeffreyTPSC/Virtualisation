import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import commons.lang;

public class buildVM extends HttpServlet {
 
  private String message;

  public void init() throws ServletException
  {
      message = "Volg de progressie van je server hier!";
  }
  
  public void doPost(HttpServletRequest request,
		    HttpServletResponse response)
	    throws ServletException, IOException
  {
	//Haal data uit de form.   
    String cpu = request.getParameter("cpu");
    String mem = request.getParameter("mem");
    String disk = request.getParameter("disk");

	public boolean isNumeric(String s) {  
	    return s.matches("[-+]?\\d*\\.?\\d+");  
		}

	if(isNumberic(cpu) == True){
	
  
	// Even test.
	System.out.println("Value van cpu is: " + cpu);
	System.out.println("Value van mem is: " + mem);
	System.out.println("Value van disk is: " + disk);

	PrintWriter writer = response.getWriter();

                //Display in html code
                String htmlResponse = "<html>";
		htmlResponse += "<h1>Je gekozen opties!</h1><br/>";
                htmlResponse += "<h2>Het aantal gekozen CPU: " + cpu + "<br/>";
                htmlResponse += "Het aantal gekozen memory: " + mem + "<br/>";
                htmlResponse += "Het aantal gekozen storage: " + disk + "</h2>";
                htmlResponse += "</html>";

                // return response
                writer.println(htmlResponse);
      
    // Set response content type
      response.setContentType("text/html");

      // Actual logic goes here.
      PrintWriter out = response.getWriter();
      out.println("<h1>" + message + "</h1>");

    try {
      String line;
      Process p = Runtime.getRuntime().exec("virt-install --connect qemu+ssh://root@10.0.0.58/system -n testerdetesten --os-type=Linux --ram="+ mem +" --vcpus="+ cpu +" --disk path=/var/lib/libvirt/images/testerdetesten.img,bus=virtio,size="+ disk +" --graphics none --cdrom /var/os/ubuntu-16.04.1-server-amd64.iso --network bridge:virbr0");
      BufferedReader bri = new BufferedReader
        (new InputStreamReader(p.getInputStream()));
      BufferedReader bre = new BufferedReader
        (new InputStreamReader(p.getErrorStream()));
      while ((line = bri.readLine()) != null) {
        out.println(line);
      }
      bri.close();
      while ((line = bre.readLine()) != null) {
        out.println(line);
      }
      bre.close();
      p.waitFor();
      out.println("Done.");
    }
    catch (Exception err) {
      err.printStackTrace();
 		   	}
	  	}
	} else
	System.out.println("Something went horribly wrong!");
}
