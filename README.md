# Virtualisation

### code voor project Virtualisatie. 
### Student: Jeffrey de Boer - HVA - ID2NE

[logo]: https://upload.wikimedia.org/wikipedia/en/thumb/f/fc/Hogeschool_van_amsterdam_logo.svg/1024px-Hogeschool_van_amsterdam_logo.svg.png "HVA"

Essentiele bestanden:
**Index.html**
Toevoegen in de root directory van nginx. In mijn geval: /usr/share/nginx/html

**buildVM.java**
Toevoegen in de webapps dir van tomcat.
Compilen deed ik met: javac -classpath %J2EE_HOME%\lib\j2ee.jar -classpath /usr/share/java/tomcat8-servlet-api.jar buildVM.java vanuit de volgende dir:
/var/lib/tomcat8/webapps/buildvm/WEB-INF/classes  (standaard class dir voor servlets)

Daarna Tomcat herstarten met:
systemctl restart tomcat8

De volgende functies werken niet:
Registreren. Geeft nog een error omdat PHP script (die logins opslaat op db) een error geeft.
Stoppen/Starten van een VM. Logica nog niet ingebouwt.
